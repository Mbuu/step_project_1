package ua.danit.controllers;

import ua.danit.model.User;
import ua.danit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static ua.danit.controllers.MainPage.currentUser;

@WebServlet (name = "ChatPage", urlPatterns = "/chat/*")
public class ChatPage extends HttpServlet {

    //private UserService userService = UserService.USER_SERVICE;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Long id = Long.parseLong(req.getParameter("userid"));
        //User user = userService.getUserById(id);

        PrintWriter out = resp.getWriter();
        out.write("<html><head>");
        out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"\n" +
                "integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">");
        out.write("</head>");
        out.write("<body style=\"background-color: lightgrey\">");
        out.write("<div align=\"center\">");
        out.write("<h1>");
        out.write("Chat");
       // out.write(user.getName());
        out.write("</h1>");
        out.write("</div></body></html>");
    }
}
