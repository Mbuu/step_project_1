package ua.danit.controllers;

import ua.danit.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LikedPage", urlPatterns = "/messages")
public class LikedPage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.write("<html><head>");
        out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"\n" +
                "integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">");
        out.write("</head>");
        out.write("<body style=\"background-color: lightgrey\">");
        out.write("<div align=\"center\">");
        out.write("<h1>");
        out.write("Liked List");
        out.write("</h1>");
        out.write("<br>");
        out.write("<hr>");
        out.write("<table width=\"80%\" border=\"1\">");

        for (User user : MainPage.loggedUser.getLiked()) {
            out.write("<tr height=\"40\" style=\"background-color: white\">");
            out.write("<td style=\"text-align: left\">");
            out.write(String.format("<a title=\"id %d\" href=\"/chat/%d\"><img height=\"80px\" width=\"57px\" src=\"%s\">     %s, %d</a>" , user.getId(), user.getId(), user.getImageUrl(), user.getName(), user.getAge()));
            out.write("</form>");
            out.write("</td>");
            out.write("</tr>");
        }

        out.write("<table>");
        out.write("</div></body></html>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


    }
}
