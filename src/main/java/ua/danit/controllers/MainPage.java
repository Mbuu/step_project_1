package ua.danit.controllers;

import ua.danit.model.User;
import ua.danit.service.UserService;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.servlet.*;


@WebServlet(name = "MainPage", urlPatterns = "/")

public class MainPage extends HttpServlet {

    private UserService userService = UserService.USER_SERVICE;

    public static User currentUser = null;
    public static User loggedUser = new User(0L, "Zhmych", 28L,
            "https://i.ytimg.com/vi/Af8aQsJCi3M/maxresdefault.jpg",
            new ArrayList<User>(),new ArrayList<User>());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            currentUser = userService.getRandomUser();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PrintWriter out = resp.getWriter();

        out.write("<html><head>");
        out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"\n" +
                "integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">");
        out.write("</head><body>");
        out.write("<div align=\"center\">");
        out.write("<img height=\"65%\" width=\"350\" src=\"");
        out.write(currentUser.getImageUrl());
        out.write("\">");
        out.write("<h3>");
        out.write(currentUser.getName());
        out.write(", " + currentUser.getAge());
        out.write("</h3>");
        out.write("<table>");
        out.write("<tr>");

        out.write("<td>");
        out.write("<form action=\"/action/yes\" method=\"post\">");
        out.write("<input type =\"hidden\" name=\"userid\" value=");
        out.write(currentUser.getId() +">");
        out.write("<button type=\"submit\" value=\"yes\" name=\"yes\">Like</button>");
        out.write("</form>");
        out.write("</td>");

        out.write("<td>");
        out.write("<form action=\"/action/no\" method=\"post\">");
        out.write("<input type =\"hidden\" name=\"userid\" value=");
        out.write(currentUser.getId() + ">");
        out.write("<button type=\"submit\" value =\"no\" name=\"no\">Dislike</button>");
        out.write("</form>");
        out.write("</td>");
        out.write("</tr>");

        out.write("<tr>");
        out.write("<td colspan=\"2\" style=\"text-align: center\">");
        out.write("<form action=\"/messages\" method=\"get\">");
        out.write("<button type=\"submit\">LikedList</button>");
        out.write("</form>");
        out.write("</td>");
        out.write("</tr>");
        out.write("</table>");
        out.write("</div></body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("post");
        /*try {
            this.currentUser = userService.getRandomUser();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        /*if (req.getParameter("yes") != null) {
            loggedUser.getLiked().add(currentUser);
        } else if (req.getParameter("no") != null) {
            loggedUser.getDisliked().add(currentUser);
        }*/

        resp.sendRedirect("/");
    }
}