package ua.danit.controllers;

import ua.danit.model.User;
import ua.danit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

import static ua.danit.controllers.MainPage.currentUser;
import static ua.danit.controllers.MainPage.loggedUser;

@WebServlet(name = "actionServlet", urlPatterns = "/action/*")
public class MainPageAction extends HttpServlet {
    private UserService userService = UserService.USER_SERVICE;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("userid"));
        User user = userService.getUserById(id);
        if ("/yes".equals(req.getPathInfo())) {
            //loggedUser.getLiked().stream().map(usser -> usser.getId()).collect(Collectors.toList());
            if (!loggedUser.getLiked().stream().map(usser -> usser.getId()).collect(Collectors.toList()).contains(user.getId()))
                loggedUser.getLiked().add(user);
        } else {
            loggedUser.getDisliked().add(user);

        }

        resp.sendRedirect("/");
    }
}
