package ua.danit.dao;

import ua.danit.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public interface UserDao {

    List<User> getAllUsers() throws SQLException;
    User getUserById(Long id) throws SQLException;

}
