package ua.danit.dao;

import ua.danit.model.User;

import java.sql.*;
import java.util.*;

public class UserDaoMock implements UserDao {

    static String DB_URL = "jdbc:mysql://localhost:3306/tinder_users?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&verifyServerCertificate=false&useSSL=true";
    static String USER = "root";
    static String PASSWD = "8888";

    @Override
    public User getUserById(Long id) throws SQLException {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
        System.out.println(id);
        String sql = "select t.id, t.name, t.age, t.image_url from users_t t where t.id = " + id;
        System.out.println(sql);
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        User user = null;

        while (resultSet.next()) {
            user = new User(resultSet.getLong("id"),
                    resultSet.getString("name"),
                    resultSet.getLong("age"),
                    resultSet.getString("image_url"));
        }
        resultSet.close();
        statement.close();
        con.close();

        return user;
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
        String sql = String.format("select id, name, age, image_url from users_t");
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        List<User> users = new ArrayList<>();

        while (resultSet.next()) {
            users.add(new User(resultSet.getLong("id"),
                    resultSet.getString("name"),
                    resultSet.getLong("age"),
                    resultSet.getString("image_url")));
        }
        resultSet.close();
        statement.close();
        con.close();

        return users;
    }


    /*public ArrayList<User> users = new ArrayList<>();

    public UserDaoMock() {
        users.add(new User(1L, "Aasty", 18L, "https://d01salon.com/wp-content/uploads/2017/07/ice-blond.jpg"));
        users.add(new User(2L, "Basty", 19L, "https://s14.favim.com/orig/161127/actress-ashley-benson-blonde-hanna-marin-Favim.com-4898149.png"));
        users.add(new User(3L, "Casty", 20L, "https://i.pinimg.com/originals/20/8b/e1/208be1b7035be758056bfefb62bbdc81.jpg"));
        users.add(new User(4L, "Dasty", 21L, "http://ciceksiparisi.site/wp-content/uploads/2018/01/medium-short-blonde-hair-21-fierce-platinum-blonde-hairstyles-to-short-medium-blonde-hairstyles-680x1024.jpg"));
        users.add(new User(5L, "Easty", 22L, "https://media.glamour.com/photos/5707b00344fb71e130aa4dd2/master/pass/lucy-hale-blond.jpg"));
        users.add(new User(6L, "Fasty", 23L, "https://i.pinimg.com/736x/64/1e/00/641e00a51b96fc2d158496d785f2211b--hair-laid-red-lips.jpg"));
        users.add(new User(7L, "Gasty", 24L, "https://images4.cosmopolitan.ru/upload/img_cache/77c/77c10619963629b79d8825bbe6147aeb_fitted_740x700.jpg"));
        users.add(new User(8L, "Hasty", 25L, "http://i42-cdn.woman.ru/womanru/images/gallery/7/f/g_7f83ceb3f25a54c794b4d692f390cc2e_2_1400x1100.jpg?02"));
        users.add(new User(9L, "Iasty", 26L, "http://lorealcastinggirls.pl/wp-content/uploads/2016/12/shutterstock_391501081.png"));
        users.add(new User(10L, "Jasty", 27L, "http://b-made.eu/wp-content/uploads/2018/01/frau-blond-haare-pony-t-2-720x540.jpg"));
    }

    @Override
    public ArrayList<User> getAllUsers() {
        return users;
    }*/

}
