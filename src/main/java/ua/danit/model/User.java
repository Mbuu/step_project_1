package ua.danit.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private Long id;
    private String name;
    private Long age;
    private String imageUrl;
    private List<User> liked;
    private List<User> disliked;

    public User(Long id, String name, Long age, String imageUrl, List<User> liked,List<User> disliked) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.imageUrl = imageUrl;
        this.liked =liked;
        this.disliked = disliked;
    }

    public User(Long id, String name, Long age, String imageUrl) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.imageUrl = imageUrl;

    }

    public List<User> getLiked() {
        return liked;
    }

    public void setLiked(List<User> liked) {
        this.liked = liked;
    }

    public List<User> getDisliked() {
        return disliked;
    }

    public void setDisliked(List<User> disliked) {
        this.disliked = disliked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", imageUrl='" + imageUrl + '\'' +
                ", liked=" + liked +
                ", disliked=" + disliked +
                '}';
    }
}
