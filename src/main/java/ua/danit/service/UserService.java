package ua.danit.service;

import ua.danit.model.User;
import ua.danit.dao.UserDaoMock;

import java.sql.SQLException;
import java.util.Random;

public class UserService {
    private UserDaoMock userDao = new UserDaoMock();
    private Random random = new Random();
    public static UserService USER_SERVICE = new UserService();

    private UserService() {
    }

    public User getRandomUser() throws SQLException {
        int randomUser = random.nextInt(userDao.getAllUsers().size());
        return userDao.getAllUsers().get(randomUser);
    }

    public User getUserById(Long id){
        try {
            return userDao.getUserById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
